package fun.mashuai.generator.util;

/**
 * @author 马帅
 * @version 1.0
 * @className StringUtil.java
 * @description TODO
 * @date 2020/3/14 15:32
 */
public class StringUtil {

    /** 下划线 */
    private static final char SEPARATOR = '_';

    /**
     * 驼峰式命名法 例如：user_name->userName
     */
    public static String toCamelCase(String s) {
        if (s == null) {
            return null;
        }
        s = s.toLowerCase();
        StringBuilder sb = new StringBuilder(s.length());
        boolean upperCase = false;
        for (int i = 0; i < s.length(); i++) {
            char c = s.charAt(i);

            if (c == SEPARATOR) {
                upperCase = true;
            }
            else if (upperCase) {
                sb.append(Character.toUpperCase(c));
                upperCase = false;
            } else {
                sb.append(c);
            }
        }
        return sb.toString();
    }

    /**
     * 首字母转大写
     */
    public static String upperFirst(String word){
        if(Character.isUpperCase(word.charAt(0))) {
            return word;
        } else {
            return (new StringBuilder()).append(Character.toUpperCase(word.charAt(0))).append(word.substring(1)).toString();
        }
    }


    public static void main(String[] args) {
        String s = "sys_message";
        System.out.println(upperFirst(toCamelCase(s)) + ".java");
    }

}
