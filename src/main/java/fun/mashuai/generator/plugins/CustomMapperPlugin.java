package fun.mashuai.generator.plugins;

import fun.mashuai.generator.plugins.core.CustomAbstractXmlElementGenerator;
import fun.mashuai.generator.plugins.core.CustomJavaMapperMethodGenerator;
import org.mybatis.generator.api.IntrospectedTable;
import org.mybatis.generator.api.PluginAdapter;
import org.mybatis.generator.api.dom.java.Interface;
import org.mybatis.generator.api.dom.java.TopLevelClass;
import org.mybatis.generator.api.dom.xml.Document;
import org.mybatis.generator.api.dom.xml.TextElement;
import org.mybatis.generator.codegen.mybatis3.javamapper.elements.AbstractJavaMapperMethodGenerator;
import org.mybatis.generator.codegen.mybatis3.xmlmapper.elements.AbstractXmlElementGenerator;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * @author 马帅
 * @version 1.0
 * @className CustomMapperPlugin.java
 * @description 自定义mapper
 * @date 2020/4/18 23:34
 */
public class CustomMapperPlugin extends PluginAdapter {

    @Override
    public boolean validate(List<String> warnings) {
        return true;
    }

    /**
     * 扩展mapper
     */
    @Override
    public boolean clientGenerated(Interface interfaze, TopLevelClass topLevelClass, IntrospectedTable introspectedTable) {
        AbstractJavaMapperMethodGenerator methodGenerator = new CustomJavaMapperMethodGenerator();
        methodGenerator.setContext(context);
        methodGenerator.setIntrospectedTable(introspectedTable);
        methodGenerator.addInterfaceElements(interfaze);
        return super.clientGenerated(interfaze, topLevelClass, introspectedTable);
    }

    /**
     * 扩展对应的xml文件
     */
    @Override
    public boolean sqlMapDocumentGenerated(Document document, IntrospectedTable introspectedTable) {
        AbstractXmlElementGenerator elementGenerator = new CustomAbstractXmlElementGenerator();
        elementGenerator.setContext(context);
        elementGenerator.setIntrospectedTable(introspectedTable);
        elementGenerator.addElements(document.getRootElement());
        document.getRootElement().addElement(new TextElement("\n<!-- ### The above code is provided by MaShuai, generated time: " + (new SimpleDateFormat("yyyy-MM-dd HH:mm:ss")).format(new Date()) + " ### -->\n"));
        document.getRootElement().addElement(new TextElement("<!-- Your codes goes here!!! -->\n\n\n"));
        return super.sqlMapDocumentGenerated(document, introspectedTable);
    }





}
