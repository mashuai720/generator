package fun.mashuai.generator.pojo;

import lombok.Data;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

/**
 * @author 马帅
 * @version 1.0
 * @className BaseEntity.java
 * @description 实体抽象类
 * @date 2020-04-17 15:24
 */
@Data
public abstract class BaseEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    /** 页数 **/
    private Integer page;

    /** 条数 **/
    private Integer limit;

    /** 排序(例如：id asc, sort desc) **/
    private String orderByClause;

    /** 创建者 **/
    private String creator;

    /** 更新者 **/
    private String updater;

    /** 搜索值 */
    private String searchValue;

    /** 请求参数 */
    private Map<String, Object> params;

    public Map<String, Object> getParams() {
        if (params == null) {
            params = new HashMap<>(16);
        }
        return params;
    }
}
